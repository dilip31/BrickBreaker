using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detroy : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.tag == "Destroy")
        {
            Destroy(collision.gameObject);
        }
    }
}
