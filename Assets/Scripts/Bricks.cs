using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bricks : MonoBehaviour
{
    [SerializeField] GameObject[] bricks;
    [SerializeField] bool MoveLeft;
    [SerializeField] [Range(0f,2.5f)] float brickDuration;

    float timer;

    private void Start()
    {
        MoveLeft = true;
        GlobalBrickManager.Instance.RegistrBricks(this);
    }
    private void Update()
    {
        timer += Time.deltaTime;
        if(timer >= brickDuration)
        {
            timer = 0f;
            bricksspawning();
            if (MoveLeft == true)
            {
               
            }
        }
    }
    public void bricksspawning()
    {
        Vector2 spawnpoint = new Vector2(Random.Range(0f, 0f), -6f);
        int birckindex = Random.Range(0, 2);

        if (GlobalBrickManager.Instance.CanMove)
        {
            Instantiate(bricks[birckindex], spawnpoint, Quaternion.identity);
        }
    }
}
