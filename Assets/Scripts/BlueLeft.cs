using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueLeft : MonoBehaviour
{
    private Character boxer;

    [SerializeField] float speedup = 1.5f;
    [SerializeField] float speedleft = 3f;
    [SerializeField] Vector3 endposition = new Vector3(-0.965f, 1f, 0f);
    private Vector3 rightend = new Vector3(-4f, 1f, 0);
    [SerializeField] bool moveup = true;

    private void Start()
    {
        boxer = GameObject.Find("CH_Body").GetComponent<Character>();
    }
    void Update()
    {
        /*if (GlobalBrickManager.Instance.CanMove)
         {

         }*/
        if (moveup == true)
        {
            if (transform.position != endposition)
            {
                transform.position = Vector3.MoveTowards(transform.position, endposition, speedup * Time.deltaTime);
            }
        }
        if (transform.position == endposition)
        {
            moveup = false;
        }
        if (moveup == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, rightend, speedleft * Time.deltaTime);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bricks")
        {
            boxer.Gameoverpanel.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
