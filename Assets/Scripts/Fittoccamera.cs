using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fittoccamera : MonoBehaviour
{
    private Camera maincamera;
    void Start()
    {
        maincamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        transform.position = new Vector3(maincamera.transform.position.x, maincamera.transform.position.y, 0);
        Vector3 bottomleft = maincamera.ViewportToWorldPoint(Vector3.zero);
        Vector3 topright = maincamera.ViewportToWorldPoint(new Vector3(maincamera.rect.width, maincamera.rect.height));
        Vector3 screensize = topright - bottomleft;
        float screenratio = screensize.x / screensize.y;
        float desiredratio = transform.localScale.x / transform.localScale.y;

        if(screenratio > desiredratio)
        {
            float height = screensize.y;
            transform.localScale = new Vector3(height * desiredratio, height);
        }
        else
        {
            float width = screensize.x;
            transform.localScale = new Vector3(width, width / desiredratio);
        }
    }
}
