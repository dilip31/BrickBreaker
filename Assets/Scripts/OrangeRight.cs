using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrangeRight : MonoBehaviour
{
    private Character boxer;

    private Animator anim;
    private Renderer render;
    [SerializeField] float speedup = 1.5f;
    [SerializeField] float speedright = 3f;
    [SerializeField] Vector3 endposition = new Vector3(0.965f, 1f, 0f);
    private Vector3 rightend = new Vector3(4f, 1f, 0);
    [SerializeField] bool moveup = true;
    private Collider2D col;

    private void Start()
    {
        boxer = GameObject.Find("CH_Body").GetComponent<Character>();
        anim = GetComponent<Animator>();
        render = GetComponent<Renderer>();
        col = GetComponent<Collider2D>();
    }
    void Update()
    {
        /*if (GlobalBrickManager.Instance.CanMove)
        {

        }*/
        if (moveup == true)
        {
            if (transform.position != endposition)
            {
                transform.position = Vector3.MoveTowards(transform.position, endposition, speedup * Time.deltaTime);
            }
        }
        if (transform.position == endposition)
        {
            moveup = false;
            //GlobalBrickManager.Instance.DisableMovement(false);
        }
        if (moveup == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, rightend, speedright * Time.deltaTime);
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bricks")
        {
            boxer.timeleft += 0.5f;
            boxer.updatescore(1);
            col.enabled = false;
            render.enabled = false;
            anim.SetTrigger("breaking");
            Destroy(gameObject,0.3f);
        }
    }
}
