using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    [SerializeField] public GameObject Gameoverpanel;
    [SerializeField] Animator anim;

    public Text scoretext;
    int score;

    public Gradient gradient;
    public Image fill;
    public Slider slider;
    public float timeleft;

    public bool stoptimer;

    private void Start()
    {
        stoptimer = true;
        Gameoverpanel.SetActive(false);
        anim = GetComponentInChildren<Animator>();
    }
    private void Update()
    {
        slider.maxValue = 30f;
        slider.value = timeleft;

        fill.color = gradient.Evaluate(slider.normalizedValue);

        if (stoptimer)
        {
            if(timeleft >= 30f)
            {
                timeleft = 30f;
            }
            if (timeleft > 0)
            {
                timeleft -= Time.deltaTime;
            }

            else
            {
                timeleft = 0;
                stoptimer = false;
                if (timeleft <= 0)
                {
                    Gameoverpanel.SetActive(true);
                    Time.timeScale = 0;
                }
            }
        }
    }
    public void updatetime(float currnttime)
    {
        currnttime += 1;

        int minutes = Mathf.FloorToInt(currnttime / 60f);
        int seconds = Mathf.FloorToInt(currnttime % 60f);
    }
    public void updatescore(int addscore)
    {
        score += addscore;
        scoretext.text = "Score : " + score;
    }
    public void left()
    {
        anim.SetTrigger("LeftArm");
        //GlobalBrickManager.Instance.DisableMovement(true);
    }
    public void right()
    {
        anim.SetTrigger("RightArm");
        //GlobalBrickManager.Instance.DisableMovement(true);
    }

}
