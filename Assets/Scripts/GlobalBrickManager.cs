using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalBrickManager : MonoBehaviour
{
    static GlobalBrickManager _instance;

    public static GlobalBrickManager Instance 
    { get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GlobalBrickManager>();
            }
            return _instance;
        }
    }

    bool canMove = true;
    public bool CanMove { get { return canMove; } }
    Bricks _bricks;
    public void DisableMovement(bool value)
    {
        canMove = value;
    }
    public void RegistrBricks(Bricks bricks)
    {
        _bricks = bricks;
    }
}
